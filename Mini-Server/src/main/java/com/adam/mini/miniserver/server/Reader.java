package com.adam.mini.miniserver.server;

import com.adam.mini.miniserver.notification.NotificationManager;
import com.adam.mini.miniserver.server.info.Request;
import com.adam.mini.miniutils.log.Log;
import com.adam.mini.miniutils.log.LogManager;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by adam on 2015/5/9.
 */
public class Reader extends Thread {
    /**
     * 读请求池
     */
    private static LinkedBlockingQueue<SelectionKey> pool
            = new LinkedBlockingQueue<SelectionKey>();
    /**
     * 通知中心
     */
    private static NotificationManager notifier
            = NotificationManager.getInstance();

    private static Log log = LogManager.getLogger();

    public Reader(){}

    @Override
    public void run() {
        while(true){
            SelectionKey key = pool.remove();
            read(key);
        }
    }



    /**
     * 默认缓存大小
     */
    private static int BUFFER_SIZE = 1024;

    /**
     * 读取字节码
     * @param sc socket通道
     * @return 读取的直接骂
     * @throws IOException IO异常
     */
    public static byte[] readRequest(SocketChannel sc) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        int off = 0;
        int r = 0;
        byte[] data = new byte[BUFFER_SIZE * 10];

        while ( true ) {
            buffer.clear();
            r = sc.read(buffer);
            if (r == -1) break;
            if ( (off + r) > data.length) {
                data = grow(data, BUFFER_SIZE * 10);
            }
            byte[] buf = buffer.array();
            System.arraycopy(buf, 0, data, off, r);
            off += r;
        }
        byte[] req = new byte[off];
        System.arraycopy(data, 0, req, 0, off);
        return req;
    }

    /**
     * 用于扩充字节数组的大小
     * @param src 原字节数组
     * @param size 扩展的大小
     * @return 扩展后的字节数组
     */
    public static byte[] grow(byte[] src, int size) {
        byte[] tmp = new byte[src.length + size];
        System.arraycopy(src, 0, tmp, 0, src.length);
        return tmp;
    }

    public void read(SelectionKey key){
        SocketChannel sc = (SocketChannel) key.channel();
        try {
            byte[] clientDate = readRequest(sc);
            Request request = (Request) key.attachment();

            request.setData(clientDate);
            notifier.notifyRead(request);

            //通知写线程可以写了
            MiniServer.processWriteRequest(key);
        } catch (IOException e) {
            e.printStackTrace();
            notifier.notifyError("读取客户端数据出错",e);
        }
    }

    public static void processRequest(SelectionKey key){
        pool.add(key);
    }


}

package com.adam.mini.miniserver.message;

import java.io.Serializable;

/**
 * Created by adam on 2015/5/12.
 */
public class Message implements Transferable, Serializable {

    /**
     * {@inheritDoc}
     * @param data
     * @return
     */
    @Override
    public Object toMessageObject(byte[] data) {
        return null;
    }

    /**
     * {@inheritDoc}
     * @param msg
     * @return
     */
    @Override
    public byte[] toMessageByte(Message msg) {
        return new byte[0];
    }
}

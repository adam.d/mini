package com.adam.mini.miniserver.server.info;

import java.net.InetAddress;
import java.nio.channels.SocketChannel;

/**
 * Created by adam on 2015/5/3.
 */
public class Request {
    private SocketChannel socketChannel;
    private byte[] data;
//    private Object attachment;

    public Request(SocketChannel socketChannel){
        this.socketChannel = socketChannel;
    }

    public SocketChannel getSocketChannel() {
        return socketChannel;
    }

    public void setSocketChannel(SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

//    public Object getAttachment() {
//        return attachment;
//    }
//
//    public void setAttachment(Object attachment) {
//        this.attachment = attachment;
//    }

    public InetAddress getAddress(){
        return socketChannel.socket().getInetAddress();
    }
}

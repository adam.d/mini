package com.adam.mini.miniserver.server;

import com.adam.mini.miniserver.notification.NotificationManager;
import com.adam.mini.miniserver.server.info.Request;
import com.adam.mini.miniserver.server.info.Response;
import com.adam.mini.miniutils.log.Log;
import com.adam.mini.miniutils.log.LogManager;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by adam on 2015/5/12.
 */
public class Writer extends Thread {

    private static LinkedBlockingQueue<SelectionKey> pool =
            new LinkedBlockingQueue<SelectionKey>();

    private static NotificationManager notifier =
            NotificationManager.getInstance();

    private static Log log = LogManager.getLogger();

    public Writer(){}

    @Override
    public void run() {
        while(true){
            SelectionKey key = pool.remove();
            write(key);
        }
    }

    private void write(SelectionKey key){
        SocketChannel socketChannel = (SocketChannel) key.channel();
        Request request = (Request) key.attachment();
        Response response = new Response(socketChannel);

        try {
            notifier.notifyWrite(request,response);
        } catch (Exception e) {
            e.printStackTrace();
            notifier.notifyError("写出错",e);
            log.e("写出错",e);
        }

        try {
            socketChannel.finishConnect();
            socketChannel.socket().close();
            socketChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
            notifier.notifyError("无法关闭连接",e);
        }

        notifier.notifyClose(request);
    }

    public static void processRequest(SelectionKey key){
        pool.add(key);
    }
}

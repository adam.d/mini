package com.adam.mini.miniserver.notification;

import com.adam.mini.miniserver.server.info.Request;
import com.adam.mini.miniserver.server.info.Response;

/**
 * Created by adam on 2015/5/3.
 */
public abstract class EventAdapter implements IEvent {
    @Override
    public void onAccept() {

    }

    @Override
    public void onAccepted(Request req) {

    }

    @Override
    public void onRead(Request req) {

    }

    @Override
    public void onWrite(Request req, Response resp) throws Exception {

    }

    @Override
    public void onClose(Request req) {

    }

    @Override
    public void onError(String msg, Throwable error) {

    }
}

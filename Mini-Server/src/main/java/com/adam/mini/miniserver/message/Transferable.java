package com.adam.mini.miniserver.message;

/**
 * Created by adam on 2015/5/12.
 */
public interface Transferable {

    /**
     * 解包装对象
     * @param data 对象字节码
     * @return
     */
    public Object toMessageObject(byte[] data);

    /**
     * 包装对象
     * @param msg 对象
     * @return
     */
    public byte[] toMessageByte(Message msg);
}

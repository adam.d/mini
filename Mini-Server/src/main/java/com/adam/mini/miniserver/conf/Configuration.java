package com.adam.mini.miniserver.conf;

import java.net.InetSocketAddress;
import java.util.Properties;

/**
 * Created by adam on 2015/5/3.
 */
public class Configuration {

    /**
     * 属性是否初始化
     */
    public boolean isInit = false;

    /**
     * 服务器IP
     */
    public InetSocketAddress serverAddress;

    /**
     * 服务器端口
     */
    public int serverPort;

    /**
     * 最大读线程数
     */
    public int MAX_READ_THREAD;

    /**
     * 最大写线程数
     */
    public int MAX_WRITE_THREAD;

    /**
     * 读线程超时时间，秒
     */
    public long MAX_READ_TIMEOUT;

    /**
     * 写线程超时时间，秒
     */
    public long MAX_WRITE_TIMEOUT;
    private Configuration(){}

    public static Configuration getDefaultConf(){
        Configuration conf = new Configuration();
        conf.serverPort = 8080;
        conf.serverAddress = new InetSocketAddress("localhost",conf.serverPort);
        conf.MAX_READ_THREAD = 10;
        conf.MAX_WRITE_THREAD = 10;
        conf.MAX_READ_TIMEOUT = 1000;
        conf.MAX_WRITE_TIMEOUT = 1000;

        conf.isInit = true;
        return conf;
    }

    public static Configuration getPropertiesConf(Properties prop){
        Configuration conf = new Configuration();
        conf.serverPort = Integer.parseInt(prop.getProperty("mini.conf.serverPort","8080"));
        conf.serverAddress = new InetSocketAddress(prop.getProperty("mini.conf.serverAddress","localhost"),
                conf.serverPort);
        conf.MAX_READ_THREAD = Integer.parseInt(prop.getProperty("mini.conf.max_read_thread","10"));
        conf.MAX_WRITE_THREAD = Integer.parseInt(prop.getProperty("mini.conf.max_write_thread","10"));
        conf.MAX_READ_TIMEOUT = Long.parseLong(prop.getProperty("mini.conf.max_read_timeout","1000"));
        conf.MAX_WRITE_TIMEOUT = Long.parseLong(prop.getProperty("mini.conf.max_write_timeout","1000"));

        conf.isInit = true;
        return conf;
    }

    public static Configuration getXmlConf(String xmlPath){

        return null;
    }
}

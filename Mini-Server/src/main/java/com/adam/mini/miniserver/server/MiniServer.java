package com.adam.mini.miniserver.server;

import com.adam.mini.miniserver.conf.Configuration;
import com.adam.mini.miniserver.notification.IEvent;
import com.adam.mini.miniserver.notification.NotificationManager;
import com.adam.mini.miniserver.server.info.Request;
import com.adam.mini.miniutils.log.Log;
import com.adam.mini.miniutils.log.LogManager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.*;

/**
 * Created by adam on 2015/5/3.
 */
public class MiniServer extends Thread implements IServer{
    private static Log log = LogManager.getLogger();
    //用于线程的停止
    private static boolean shutdown = false;
    //判断线程是否停止
    private static boolean isShutDown = false;
    //用于存储写请求
    private static LinkedBlockingQueue<SelectionKey> wpool
            = new LinkedBlockingQueue<SelectionKey>();
    //读写线程池
    private ExecutorService wgroup;
    private ExecutorService rgroup;
//    private ThreadPoolExecutor wgroup;
//    private ThreadPoolExecutor rgroup;
    //用于阻塞客户端请求
//    private ArrayBlockingQueue<Runnable> wblock;
//    private ArrayBlockingQueue<Runnable> rblcok;

    private static Selector selector;
    private ServerSocketChannel ssc;

    private InetSocketAddress socketAddress;
    private int port;

    private String serverName;

    private NotificationManager notifier = NotificationManager.getInstance();

    /**
     * 使用默认的配置文件启动服务
     * @param name 命名
     */
    public MiniServer(String name) throws IOException {
        this(name,Configuration.getDefaultConf());
    }

    /**
     * 使用自定义的配置文件启动服务
     * @name 命名
     * @param conf 配置文件对象
     */
    public MiniServer(String name,Configuration conf) throws IOException {
        serverName = name;
        wgroup = Executors.newFixedThreadPool(conf.MAX_WRITE_THREAD);
//        wblock = new ArrayBlockingQueue<Runnable>(conf.MAX_WRITE_THREAD);
//        wgroup = new ThreadPoolExecutor(conf.MAX_WRITE_THREAD,conf.MAX_WRITE_THREAD,
//                conf.MAX_WRITE_TIMEOUT,TimeUnit.SECONDS,wblock,new ThreadPoolExecutor.CallerRunsPolicy());
        log.i("创建完写线程池");
        rgroup = Executors.newFixedThreadPool(conf.MAX_READ_THREAD);
//        rblcok = new ArrayBlockingQueue<Runnable>(conf.MAX_READ_THREAD);
//        rgroup = new ThreadPoolExecutor(conf.MAX_READ_THREAD,conf.MAX_READ_THREAD,
//                conf.MAX_READ_TIMEOUT,TimeUnit.SECONDS,rblcok,new ThreadPoolExecutor.CallerRunsPolicy());
        log.i("创建完读线程池");

        selector = Selector.open();
        ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);
        ServerSocket serverSocket = ssc.socket();
        serverSocket.bind(conf.serverAddress);
        ssc.register(selector,SelectionKey.OP_ACCEPT);
    }

    /**
     * {@inheritDoc}
     * 对外开放接口
     */
    @Override
    public void startServer() {
        this.start();
//        log.i("服务器启动");
    }

    /**
     * {@inheritDoc}
     * @param delayTime 延迟时间 毫秒
     */
    @Override
    public void startServer(long delayTime) {
        try {
            Thread.sleep(delayTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
            log.e("延迟启动失败",e);
            return;
        }

        startServer();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destory() throws IOException {
        shutdown = true;

        while(!isShutDown){
            log.i("等待线程关闭");
        }
        //关闭socket
        ssc.close();
        ssc.socket().close();
        selector.close();
        //清空写存储
        wpool.clear();
        //关闭线程池
        wgroup.shutdown();
        rgroup.shutdown();
        log.i("服务器关闭");
    }

    /**
     * {@inheritDoc}
     * @param IEvent 事件监听器
     */
    @Override
    public void addEventListener(IEvent IEvent) {
        notifier.addListener(IEvent);
    }

    @Override
    public boolean isRunning() {
        return !isShutDown;
    }

    @Override
    public void run() {
        log.i("服务器启动");
        while(!shutdown){
            try {
                int num=0;
                num = selector.select();

                if (num>0){
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iter = selectionKeys.iterator();

                    while(iter.hasNext()){
                        SelectionKey sKey = iter.next();
                        iter.remove();

                        if ((sKey.readyOps() & SelectionKey.OP_ACCEPT)==SelectionKey.OP_ACCEPT){
                            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) sKey.channel();
                            notifier.notifyAccept();

                            SocketChannel socketChannel = serverSocketChannel.accept();
                            socketChannel.configureBlocking(false);

                            Request request = new Request(socketChannel);
                            notifier.notifyAccepted(request);

                            socketChannel.register(selector,SelectionKey.OP_READ,request);

                            log.i("On Accepted");
                        }else if((sKey.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ){
                            rgroup.submit(new Reader());
                            Reader.processRequest(sKey);
                            sKey.cancel();
                            log.i("On Read");
                        }else if ((sKey.readyOps() & SelectionKey.OP_WRITE) == SelectionKey.OP_WRITE){
                            wgroup.submit(new Writer());
                            Writer.processRequest(sKey);
                            sKey.cancel();
                            log.i("On Writed");
                        }
                    }
                }else{
                    addRegister();
                }
            } catch (IOException e) {
                e.printStackTrace();
                log.e("线程循环体出错",e);
                notifier.notifyError("线程循环体出错",e);
            }
        }

        isShutDown = true;
    }

    private void addRegister(){
//        log.i("On Add Register");
        while(!wpool.isEmpty()){
            SelectionKey key = wpool.remove();
            SocketChannel socketChannel = (SocketChannel) key.channel();
            try {
                socketChannel.register(selector,SelectionKey.OP_WRITE,key.attachment());
            } catch (ClosedChannelException e) {
                e.printStackTrace();
                log.e("注册写事件失败",e);
                try {
                    socketChannel.finishConnect();
                    socketChannel.socket().close();
                    notifier.notifyClose((Request)key.attachment());
                } catch (IOException e1) {
                    e1.printStackTrace();
                    notifier.notifyError("无法挂壁连接",e1);
                }

            }
        }
    }

    public static void processWriteRequest(SelectionKey key){
        wpool.add(key);
//        wpool.notifyAll();
        selector.wakeup();
    }
}

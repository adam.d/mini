package com.adam.mini.miniserver.notification;

import com.adam.mini.miniserver.server.info.Request;
import com.adam.mini.miniserver.server.info.Response;
import com.adam.mini.miniutils.log.Log;
import com.adam.mini.miniutils.log.LogManager;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by adam on 2015/5/3.
 * 通知中心为一个单例，启动所有的监听器
 */
public class NotificationManager {
    private static List<IEvent> listeners;
    private static NotificationManager manager;
    private ReentrantLock lock;
    private Log log = LogManager.getLogger();

    private NotificationManager(){
        listeners = new LinkedList<IEvent>();
        lock = new ReentrantLock();

        log.d(NotificationManager.class.getName()+" started ");
    }

    /**
     * 获取该实例
     * @return
     */
    public static synchronized NotificationManager getInstance(){
        if (manager == null) manager = new NotificationManager();

        return manager;
    }

    /**
     * 添加监听器
     * @param l 具体监听器
     */
    public void addListener(IEvent l){
//        synchronized(listeners){
//            if (!listeners.contains(l))
//                listeners.add(l);
//        }
        lock.lock();
        try{
            if (!listeners.contains(l))
                listeners.add(l);
        }finally {
            lock.unlock();
        }

        log.d(NotificationManager.class.getName()+" had add listener "+l.getClass().getName());
    }

    /**
     * 通知接收事件
     */
    public void notifyAccept(){
        Iterator<IEvent> iter = listeners.iterator();
        while(iter.hasNext())
            iter.next().onAccept();

        log.d(NotificationManager.class.getName()+" had notify accept events");
    }

    /**
     * 通知接收事件
     * @param req 请求
     */
    public void notifyAccepted(Request req){
        Iterator<IEvent> iter = listeners.iterator();
        while(iter.hasNext()){
            iter.next().onAccepted(req);
        }

        log.d(NotificationManager.class.getName()+" had notify accepted events");
    }

    /**
     * 通知读取事件
     * @param req 请求
     */
    public void notifyRead(Request req){
        Iterator<IEvent> iter = listeners.iterator();
        while(iter.hasNext()){
            iter.next().onRead(req);
        }

        log.d(NotificationManager.class.getName()+" had notify read events");
    }

    /**
     * 通知写事件
     * @param req 请求
     * @param resp 应答
     */
    public void notifyWrite(Request req,Response resp) throws Exception {
        Iterator<IEvent> iter = listeners.iterator();
        while(iter.hasNext()){
            iter.next().onWrite(req,resp);
        }

        log.d(NotificationManager.class.getName()+" had notify write events");
    }

    /**
     * 通知关闭事件
     * @param req 请求
     */
    public void notifyClose(Request req){
        Iterator<IEvent> iter = listeners.iterator();
        while(iter.hasNext()){
            iter.next().onClose(req);
        }

        log.d(NotificationManager.class.getName()+" had notify close events");
    }

    /**
     * 通知错误事件
     * @param error 错误
     */
    public void notifyError(String msg,Throwable error){
        Iterator<IEvent> iter = listeners.iterator();
        while(iter.hasNext()){
            iter.next().onError(msg,error);
        }

        log.d(NotificationManager.class.getName()+" had notify error events");
    }
}

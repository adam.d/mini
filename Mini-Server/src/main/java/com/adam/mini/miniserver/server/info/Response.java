package com.adam.mini.miniserver.server.info;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by adam on 2015/5/3.
 */
public class Response {
    private SocketChannel socketChannel;

    public Response(SocketChannel socketChannel){
        this.socketChannel = socketChannel;
    }

    public void send(byte[] data) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.length);
        buffer.put(data,0,data.length);
        buffer.flip();
        socketChannel.write(buffer);
    }
}

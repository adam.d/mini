package com.adam.mini.miniserver.server.life;

import com.adam.mini.miniserver.server.Context;

/**
 * Created by adam on 2015/5/3.
 */
public abstract class LifeAdapter implements ILifeCircle {
    @Override
    public boolean beforeStart(Context context) {
        return false;
    }

    @Override
    public void afterStart(Context context) {

    }

    @Override
    public void beforeDestory(Context context) {

    }

    @Override
    public void afterDestroy() {

    }
}

package com.adam.mini.miniserver.server;

import com.adam.mini.miniserver.notification.IEvent;

import java.io.IOException;

/**
 * Created by adam on 2015/5/3.
 *
 * 服务端接口
 */
public interface IServer {
    /**
     * 启动服务
     */
    public void startServer();

    /**
     * 延迟启动服务
     * @param delayTime 延迟时间
     */
    public void startServer(long delayTime);

    /**
     * 服务销毁回调接口
     */
    public void destory() throws IOException;

    /**
     * 添加事件监听器
     * @param IEvent 事件监听器
     */
    public void addEventListener(IEvent IEvent);

    public boolean isRunning();
}

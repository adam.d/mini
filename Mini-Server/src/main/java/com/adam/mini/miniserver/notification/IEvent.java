package com.adam.mini.miniserver.notification;

import com.adam.mini.miniserver.server.info.Request;
import com.adam.mini.miniserver.server.info.Response;

/**
 * Created by adam on 2015/5/3.
 * 请求时间回调接口
 */
public interface IEvent {

    /**
     * 在请求接收前发生
     */
    public void onAccept();

    /**
     * 在请求接收后发生
     * @param req
     */
    public void onAccepted(Request req);

    /**
     * 处理读事件
     * @param req
     */
    public void onRead(Request req);

    /**
     * 处理写事件
     * @param req
     * @param resp
     */
    public void onWrite(Request req,Response resp) throws Exception;

    /**
     * 处理关闭事件
     * @param req
     */
    public void onClose(Request req);

    /**
     * 处理错误事件
     * @param error
     */
    public void onError(String msg,Throwable error);
}

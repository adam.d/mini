package com.adam.mini.miniserver.server.life;

import com.adam.mini.miniserver.server.Context;

/**
 * Created by adam on 2015/5/3.
 * 用户监听服务器的生命周期
 */
public interface ILifeCircle {

    /**
     * 在服务器启动前
     * @param context 上下文对象
     * @return true 继续，否则启动失败
     */
    public boolean beforeStart(Context context);

    /**
     * 服务器启动后
     * @param context 上下文对象
     */
    public void afterStart(Context context);

    /**
     * 在服务器销毁前
     * @param context 上下文对象
     */
    public void beforeDestory(Context context);

    /**
     * 在服务器销毁后
     */
    public void afterDestroy();
}

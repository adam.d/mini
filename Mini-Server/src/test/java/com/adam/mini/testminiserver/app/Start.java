package com.adam.mini.testminiserver.app;


import com.adam.mini.miniserver.notification.NotificationManager;
import com.adam.mini.miniserver.server.MiniServer;

/**
 * <p>Title: ������</p>
 * @author starboy
 * @version 1.0
 */

public class Start {

    public static void main(String[] args) {
        try {
            LogHandler loger = new LogHandler();
            TimeHandler timer = new TimeHandler();
            NotificationManager notifier = NotificationManager.getInstance();
            notifier.addListener(loger);
            notifier.addListener(timer);

            System.out.println("Server starting ...");
            MiniServer server = new MiniServer("mini");
            server.start();
        }
        catch (Exception e) {
            System.out.println("Server error: " + e.getMessage());
            System.exit(-1);
        }
    }
}

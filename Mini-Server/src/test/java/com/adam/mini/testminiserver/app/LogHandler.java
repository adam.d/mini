package com.adam.mini.testminiserver.app;

import com.adam.mini.miniserver.notification.EventAdapter;
import com.adam.mini.miniserver.server.info.Request;

import java.util.Date;

/**
 * ��־��¼
 */
public class LogHandler extends EventAdapter {
    public LogHandler() {
    }

    public void onClosed(Request request) throws Exception {
        String log = new Date().toString() + " from " + request.getAddress().toString();
        System.out.println(log);
    }

    public void onError(String error) {
        System.out.println("Error: " + error);
    }
}

package com.adam.mini.miniutils.exception;

/**
 * Created by lab on 2015/5/6.
 */
public class MethodNotImplementException extends RuntimeException {

    public MethodNotImplementException(){
        super();
    }

    public MethodNotImplementException(String msg){
        super(msg);
    }

    public MethodNotImplementException(Throwable throwable){
        super(throwable);
    }
}

package com.adam.mini.miniutils.log;

import com.adam.mini.miniutils.log.impl.ConsoleLog;

/**
 * Created by lab on 2015/5/6.
 */
public class LogManager {

    public enum Logger{
        Console,Android
    }

    public static Log getLogger(){
        return getLogger(Logger.Console);
    }

    public static Log getLogger(Logger logger){
        Log log = null;

        switch(logger){
            case Console :{
                log = ConsoleLog.getInstance();
                break;
            }
        }

        return log;
    }

}

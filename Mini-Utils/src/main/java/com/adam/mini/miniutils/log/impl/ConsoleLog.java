package com.adam.mini.miniutils.log.impl;

import com.adam.mini.miniutils.exception.MethodNotImplementException;
import com.adam.mini.miniutils.log.Log;
import org.apache.log4j.Logger;



/**
 * Created by lab on 2015/5/6.
 */
public class ConsoleLog implements Log{
    private static Logger logger = Logger.getLogger(ConsoleLog.class);
    private static ConsoleLog instance;

    public synchronized static ConsoleLog getInstance(){
        if (instance == null) instance = new ConsoleLog();

        return instance;
    }

    @Override
    public void i(String msg) {
        logger.info(msg);
    }

    @Override
    public void i(String format, Object... args) {
        logger.info(createMsg(format,args));
    }

    @Override
    public void v(String msg) {
        throw new MethodNotImplementException();
    }

    @Override
    public void v(String format, Object... args) {
        throw new MethodNotImplementException();
    }

    @Override
    public void d(String msg) {
        logger.debug(msg);
    }

    @Override
    public void d(String format, Object... args) {
        logger.debug(createMsg(format,args));
    }

    @Override
    public void w(String msg) {
        logger.warn(msg);
    }

    @Override
    public void w(String format, Object... args) {
        logger.warn(createMsg(format,args));
    }

    @Override
    public void e(String msg) {
        logger.error(msg);
    }

    @Override
    public void e(String format, Object... args) {
        logger.error(createMsg(format,args));
    }

    @Override
    public void e(String msg,Exception e) {
        logger.error(msg,e);
    }

    private String createMsg(String format,Object... args){
        return String.format(format,args);
    }
}

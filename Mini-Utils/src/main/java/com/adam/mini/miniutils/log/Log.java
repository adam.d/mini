package com.adam.mini.miniutils.log;

/**
 * Created by lab on 2015/5/6.
 */
public interface Log {

    /**
     *info
     */
    public void i(String msg);

    /**
     * info
     */
    public void i(String format,Object... args);

    /**
     * verbor
     */
    public void v(String msg);

    /**
     * verbo
     */
    public void v(String format,Object... args);

    /**
     * debug
     */
    public void d(String msg);

    /**
     * debug
     */
    public void d(String format,Object... args);

    /**
     * warning
     */
    public void w(String msg);

    /**
     * warning
     */
    public void w(String format,Object... args);

    /**
     * error
     */
    public void e(String msg);

    /**
     * error
     */
    public void e(String format,Object... args);

    /**
     * error
     */
    public void e(String msg,Exception e);
}
